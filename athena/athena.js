const AWS = require('aws-sdk');
const _ = require('lodash');
const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY } = require('../config');

const ATHENA_DB = 'scarif-dev';
const ATHENA_OUTPUT_LOCATION = 's3://aws-athena-query-results-443555584785-us-east-1/notifications-poc';
const RESULT_SIZE = 1000;
const POLL_INTERVAL = 3000;

const client = new AWS.Athena({region: 'us-east-1', accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_SECRET_ACCESS_KEY});

const query = 'SELECT * FROM capture_users_report where ostype = \'win\';';

async function start(){
    const namedQueryId = await client.createNamedQuery({
        Database: ATHENA_DB,
        Name: 'top_10_capture_users_report',
        QueryString: query
    }).promise();
    
    console.log(`NamedQueryId is: ${namedQueryId.NamedQueryId}`);

    const namedQuery = await client.getNamedQuery({
        NamedQueryId: namedQueryId.NamedQueryId
    }).promise();    
    
    console.log(`Retrieved NamedQuery: ${namedQuery.NamedQuery.QueryString} for NamedQueryId: ${namedQueryId.NamedQueryId}`);

    const queryExecutionResult = await client.startQueryExecution({
        QueryString: namedQuery.NamedQuery.QueryString,
        QueryExecutionContext: {
            Database: namedQuery.NamedQuery.Database
        },
        ResultConfiguration: {
            OutputLocation: ATHENA_OUTPUT_LOCATION
        }
    }).promise();
    console.log(`QueryExecutionId after execution of the query: ${queryExecutionResult.QueryExecutionId}`);
    
    console.log(`Poll till the query execution gets completed`);
    console.time(`Polling time`);    
    await startPolling(queryExecutionResult.QueryExecutionId);
    console.timeEnd(`Polling time`);

    console.log(`Retrieving query results using the QueryExecutionId: ${queryExecutionResult.QueryExecutionId}`);

    console.time(`Total retrieval time`);
    const results = await buildResults(queryExecutionResult.QueryExecutionId);    
    console.timeEnd(`Total retrieval time`);
    console.log(`Total items retrieved: ${results.length}`);
    return results;
}


async function startPolling(id) {
    return new Promise((resolve, reject) => {
        function poll(id) {
            client.getQueryExecution({QueryExecutionId: id}, (err, data) => {
                if (err) return reject(err)
                if (data.QueryExecution.Status.State === 'SUCCEEDED') return resolve(id)
                else if (['FAILED', 'CANCELLED'].includes(data.QueryExecution.Status.State)) return reject(new Error(`Query ${data.QueryExecution.Status.State}`))
                else { setTimeout(poll, POLL_INTERVAL, id) }
            })
        }
        poll(id)
    })
}

async function buildResults(query_id, max, page) {
    let max_num_results = max ? max : RESULT_SIZE
    let page_token = page ? page : undefined
    return new Promise((resolve, reject) => {
        let params = {
            QueryExecutionId: query_id,
            MaxResults: max_num_results,
            NextToken: page_token
        }

        let counter = 1;
        let dataBlob = []
        go(params)

        /* Get results and iterate through all pages */
        function go(param) {
            console.time(`page ${counter} retrieval time`);
            getResults(param)
            .then((res) => {
                console.timeEnd(`page ${counter} retrieval time`);
                dataBlob = _.concat(dataBlob, res.list)
                if (res.next) {
                    counter++;
                    param.NextToken = res.next
                    return go(param)
                } else return resolve(dataBlob)
            }).catch((err) => { return reject(err) })
        }

        /* Process results merging column names and values into a JS object */
        function getResults() {
            return new Promise((resolve, reject) => {
                client.getQueryResults(params, (err, data) => {
                    if (err) return reject(err)
                    var list = []
                    let header = buildHeader(data.ResultSet.ResultSetMetadata.ColumnInfo)
                    let top_row = _.map(_.head(data.ResultSet.Rows).Data, (n) => { return n.VarCharValue })
                    let resultSet = (_.difference(header, top_row).length > 0) ?
                        data.ResultSet.Rows :
                        _.drop(data.ResultSet.Rows)
                    resultSet.forEach((item) => {
                        list.push(_.zipObject(header, _.map(item.Data, (n) => {return n.VarCharValue })))
                    })
                    return resolve({next: ('NextToken' in data) ? data.NextToken : undefined, list: list})
                })
            })
        }
    })
}

function buildHeader(columns) {
    return _.map(columns, (i) => { return i.Name })
}

start()
.then(res => console.log(res))
.catch(err => console.log(err));
