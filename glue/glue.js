const AWS = require('aws-sdk');
const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY } = require('../config');

const glue = new AWS.Glue({ region: 'us-east-1', accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_SECRET_ACCESS_KEY });

// const query = 'SELECT * FROM capture_users_report where country = \'Belize\';';
const jobName = 'notifications_poc';

const startTime = Date.now() / 1000;
const log = (msg) => {
	const logMsg = `${new Date().toLocaleTimeString()} [${(Date.now() / 1000 - startTime).toFixed(1)}] ${msg}`;
	console.log(logMsg);
}

const startJob = async () => { 
    return glue.startJobRun({
        JobName: jobName,
        Arguments: {
            '--table': 'capture_users_report',
            '--where': 'country = \'Belize\''
        }
    }).promise();
}

const pollJobStatus = async(jobRunId) => {
    return new Promise(async (resolve, reject) => {
        async function poll(jobRunId){
            const data = await glue.getJobRun({JobName: jobName, RunId: jobRunId}).promise();
            const jobRunState = data.JobRun.JobRunState;
            if(jobRunState === 'SUCCEEDED'){
                resolve(`job ${jobRunId} completed in ${data.JobRun.ExecutionTime} seconds`);
            }
            else{
                if(jobRunState === 'STOPPED' || jobRunState === 'FAILED' || jobRunState === 'TIMEOUT'){
                    reject(`job ${jobRunId} ${jobRunState} with the error: ${data.JobRun.ErrorMessage}`);
                }
                else{
                    log(`job ${jobRunId} is ${jobRunState}`);
                    setTimeout(poll, 30000, jobRunId);
                }
            }            
        }
        await poll(jobRunId);        
    });
}

const start = async () => {
    const { JobRunId : jobRunId } = await startJob();
    log(`started job ${jobRunId}`);
    return await pollJobStatus(jobRunId);    
}

start()
.then(res => console.log(res))
.catch(err => console.log(err));