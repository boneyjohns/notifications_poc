import sys
import json
import boto3
import logging
from pyspark.sql.utils import AnalysisException
from pyspark.sql.types import StringType, StructType, StructField, IntegerType, TimestampType, DateType
from pyspark.sql.functions import udf, year, month, get_json_object, col, count, max as max, min as min, sum as sum, first as firstF, last as lastF, when, lit
from datetime import datetime, timedelta, date
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

args = getResolvedOptions(sys.argv, ['JOB_NAME', 'table', 'where'])
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
logger = glueContext.get_logger()
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

logger.info("started processing")
mapping = {
    'capture_users_report': "s3://notifications-poc-boney"
}
table = args['table']
where = args['where']


logger.info("table is {}".format(table))
logger.info("where clause is {}".format(where))

#source_location = "s3://notifications-poc-boney"
source_location = mapping.get(table)
logger.info("source location is {}".format(source_location))


df = spark.read.parquet(source_location)
logger.info(str(df.count()))

df.createOrReplaceTempView("view")
df = spark.sql("select * from view where {}".format(where))
##df = df.filter(df['country'] == "Belize")
logger.info(str(df.count()))

dyF = DynamicFrame.fromDF(df, glueContext, "dynamic")

glueContext.write_dynamic_frame.from_options(frame = dyF, connection_type = "dynamodb", connection_options = {"tableName": "notification_poc"})
logger.info("ended processing")